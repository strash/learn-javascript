var AVA = document.getElementById('ava').style;
var IMG = document.getElementById('images').style;
var PAGE1 = document.getElementById('page1').style;
var PAGE2 = document.getElementById('page2').style;
var PAGE3 = document.getElementById('page3').style;
var PAGE4 = document.getElementById('page4').style;
var COUNTER = 966;

window.onload = function () {
  document.getElementById('auto-year').innerHTML = '&copy;&nbsp;2014&ndash;' + new Date().getFullYear() + '. Все своими руками.&nbsp;<a href="http://strash.ru" target="_blank" title="Автор">strash.ru</a>';
}

/* ------------- красим лого при ховере -------------- */
function paintAva() {
  'use strict';
  var r, g, b = Math.floor(Math.random() * 256);

  AVA.color = 'rgb(' + r + ',' + g + ',' + b + ')';
}

function paintAvaBack() {
  'use strict';
  AVA.color = '';
}

/* ------------- карусель -------------- */
function moveImg() {
  'use strict';
  IMG.marginLeft = '-' + COUNTER + 'px';
  COUNTER = COUNTER + 966;
  if (COUNTER >= 3864) {
    COUNTER = 0;
  }

  if (COUNTER === 0) {
    PAGE1.backgroundColor = '#fff';
    PAGE2.backgroundColor = '';
    PAGE3.backgroundColor = '';
    PAGE4.backgroundColor = '#c4c4c4';
  }

  if (COUNTER === 966) {
    PAGE1.backgroundColor = '#c4c4c4';
    PAGE2.backgroundColor = '';
    PAGE3.backgroundColor = '';
    PAGE4.backgroundColor = '';
  }

  if (COUNTER === 1932) {
    PAGE1.backgroundColor = '#fff';
    PAGE2.backgroundColor = '#c4c4c4';
    PAGE3.backgroundColor = '';
    PAGE4.backgroundColor = '';
  }

  if (COUNTER === 2898) {
    PAGE1.backgroundColor = '#fff';
    PAGE2.backgroundColor = '';
    PAGE3.backgroundColor = '#c4c4c4';
    PAGE4.backgroundColor = '';
  }
}

function goTo1() {
  'use strict';
  IMG.marginLeft = '-' + '0' + 'px';
  PAGE1.backgroundColor = '#c4c4c4';
  PAGE2.backgroundColor = '';
  PAGE3.backgroundColor = '';
  PAGE4.backgroundColor = '';
}

function goTo2() {
  'use strict';
  IMG.marginLeft = '-' + '966' + 'px';
  PAGE1.backgroundColor = '#fff';
  PAGE2.backgroundColor = '#c4c4c4';
  PAGE3.backgroundColor = '';
  PAGE4.backgroundColor = '';
}

function goTo3() {
  'use strict';
  IMG.marginLeft = '-' + '1932' + 'px';
  PAGE1.backgroundColor = '#fff';
  PAGE2.backgroundColor = '';
  PAGE3.backgroundColor = '#c4c4c4';
  PAGE4.backgroundColor = '';
}

function goTo4() {
  'use strict';
  IMG.marginLeft = '-' + '2898' + 'px';
  PAGE1.backgroundColor = '#fff';
  PAGE2.backgroundColor = '';
  PAGE3.backgroundColor = '';
  PAGE4.backgroundColor = '#c4c4c4';
}