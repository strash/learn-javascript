/*jslint plusplus: true */
var SITEHEAD = document.getElementById('siteHead').style;
var AVA = document.getElementById('ava').style;
var HEADH1 = document.getElementById('headH1').style;
var HEADH5 = document.getElementById('headH5').style;
var HEADP = document.getElementById('headP').style;
var BGTONE = document.getElementById('bgTone').style;
var HEADBUTGO = document.getElementById('headButGo').style;
var HEADBUTBACK = document.getElementById('headButBack').style;
var IMGNUM = Math.floor(Math.random() * (25 - 1 + 1) + 1);

/* ------------- поведение в шапке -------------- */
function setAvaMargin() {
  'use strict';
  SITEHEAD.height = '666px';
  HEADH1.top = '-200px';
  HEADH5.top = '-200px';
  HEADP.top = '-102px';
  HEADP.paddingTop = '142px';
  HEADP.height = '524px';
  HEADP.opacity = '1';
  BGTONE.opacity = '.7';
  HEADBUTGO.display = 'none';
  HEADBUTBACK.display = 'inline-block';
  SITEHEAD.backgroundImage = 'url(http://js.strash.ru/img/bg/bg_' + IMGNUM + '_blur.jpg)';
}

function goAvaBack() {
  'use strict';
  SITEHEAD.height = '';
  HEADH1.top = '';
  HEADH5.top = '';
  HEADP.top = '';
  HEADP.opacity = '';
  BGTONE.opacity = '';
  HEADBUTGO.display = '';
  HEADBUTBACK.display = '';
  SITEHEAD.backgroundImage = 'url(http://js.strash.ru/img/bg/bg_' + IMGNUM + '.jpg)';
}

function setArticles(id) {
  'use strict';
  document.getElementById(id).innerHTML = '<li><a href="/articles/schetchik-kotoryy-menyayet-na-vydache-formulirovku.html">Счетчик, который меняет на выдаче формулировку</a></li><li><a href="/articles/fokusy-s-knopkami.html">Фокусы с кнопками</a></li><li><a href="/articles/po-kliku-na-knopku-krasitsya-paragraf.html">По клику на кнопку красится параграф</a></li><li><a href="/articles/praktika-s-confirm-prompt-i-alert.html">Практика с confirm, prompt и alert</a></li><li><a href="/articles/uslovnyye-operatory-if-else-if-i-ternarnyy-operator.html">Условные операторы if, else if, ?</a></li><li><a href="/articles/logicheskiye-operatory.html">Логические операторы ||, &amp;&amp; и !</a></li><li><a href="/articles/tsikly-while-do-while-i-for.html">Циклы while, do while и for</a></li><li><a href="/articles/direktivy-break-continue-i-metki.html">Директивы break, continue и метки</a></li><li><a href="/articles/konstruktsiya-switch.html">Конструкция switch</a></li><li><a href="/articles/funktsiya.html">Функция</a></li><li><a href="/articles/kak-menyat-polozheniye-elementov-pri-navedenii.html">Как менять положение элементов при наведении</a></li><li><a href="/articles/rekursiya.html">Рекурсия</a></li><li><a href="/articles/stroki.html">Строки</a></li><li><a href="/articles/chisla.html">Числа</a></li><li><a href="/articles/obyekty.html">Объекты</a></li><li><a href="/articles/massivy.html">Массивы</a></li><li><a href="/articles/massivy-metody.html">Массивы: методы</a></li><li><a href="/articles/data-i-vremya.html">Дата и время</a></li>';
}

window.onload = function () {
  'use strict';
  if (document.getElementById('sideArticles') !== null) {
    setArticles('sideArticles');
  }
  SITEHEAD.backgroundImage = 'url(http://js.strash.ru/img/bg/bg_' + IMGNUM + '.jpg)';
  if (document.getElementById('input15') !== null) {
    document.getElementById('input15').style.width = '40px';
  }
  if (document.getElementById('input16') !== null) {
    document.getElementById('input16').style.width = '40px';
  }
  if (document.getElementById('input16_2') !== null) {
    document.getElementById('input16_2').style.width = '40px';
  }
  if (document.getElementById('input17') !== null) {
    document.getElementById('input17').style.width = '40px';
  }
  if (document.getElementById('input20') !== null) {
    document.getElementById('input20').style.width = '470px';
  }
  if (document.getElementById('input21') !== null) {
    document.getElementById('input21').style.width = '100px';
  }
  document.getElementById('auto-year').innerHTML = '&copy;&nbsp;2014&ndash;' + new Date().getFullYear() + '. Все своими руками.&nbsp;<a href="http://strash.ru" target="_blank" title="Автор">strash.ru</a>';
};

/* ------------- красим лого при ховере -------------- */
function paintAva() {
  'use strict';
  var r = Math.floor(Math.random() * 256), g = Math.floor(Math.random() * 256), b = Math.floor(Math.random() * 256);
  AVA.color = 'rgb(' + r + ',' + g + ',' + b + ')';
}

function paintAvaBack() {
  'use strict';
  AVA.color = '';
}

/* ------------- проверка на число -------------- */
function isNumber(n) {
  'use strict';
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/* ------------- custom alert -------------- */
function myAlert(text, time) {
  'use strict';
  var i;
  for (i = 0; i < time; i++) {
    document.getElementById('modal_overlay').style.opacity = '.1';
    document.getElementById('modal_overlay').style.visibility = 'visible';
    document.getElementById('alert').style.opacity = '1';
    document.getElementById('alert').style.visibility = 'visible';
    document.getElementById('alertContent').innerHTML = '<p>' + text + time + '</p>';
  }
}

function closeAlert() {
  'use strict';
  document.getElementById('modal_overlay').style.opacity = '0';
  document.getElementById('modal_overlay').style.visibility = 'hidden';
  document.getElementById('alert').style.opacity = '0';
  document.getElementById('alert').style.visibility = 'hidden';
}


// старый и неуклюжий код на наведение шапки
//function setAvaMargin() {
//  var elem = document.getElementById('siteHead');
//  var elemAva = document.getElementById('ava');
//  var str = getComputedStyle(elem, '').height; //переменная высоты шапки. строка 'число px'
//  var strLength = getComputedStyle(elem, '').height.length - 2; //переменная количества символов в строке минус 'px'
//  var docHeight = document.documentElement.clientHeight*60/100;
//  var avaPos = docHeight - 75;
//  var textPos = docHeight/2 - 100;
//
//  elem.style.height = docHeight + 'px';
//  elemAva.style.top = avaPos + 'px';
//  elemAva.style.width = 150 + 'px';
//  elemAva.style.height = 150 + 'px';
//  document.getElementById('mainText').style.paddingTop = textPos + 'px';
//};

//function goAvaBack() {
//  document.getElementById('siteHead').style.height = '';	//возвращаем на место
//  document.getElementById('ava').style.top = '';	//возвращаем на место
//  document.getElementById('ava').style.width = '';
//  document.getElementById('ava').style.height = '';
//  document.getElementById('mainText').style.paddingTop = '';	//возвращаем на место
//};






// ——————— счетчик, который меняет на выдаче формулировку ——————— //
function counter() {
  'use strict';
  var i;
  for (i = 1; i <= 5; i++) {
    while (i > 1 && i < 5) {
      alert(i + ' раза');
      document.getElementById('firstInput').value = 'Алерт показан ' + i + ' раза';
      i++;
    }
    alert(i + ' раз');
    document.getElementById('firstInput').value = 'Алерт показан ' + i + ' раз';
  }
}

// ——————— фокусы с кнопкками ——————— //
if (document.getElementById('baton1') !== null) {
  document.getElementById('baton1').onclick = function () {
    'use strict';
    document.getElementById('baton2').style.visibility = 'visible';
    document.getElementById('baton1').style.visibility = 'hidden';
    document.getElementById('baton').setAttribute('value', 'button');
  };
}

// ——————— фокусы с кнопкками ——————— //
if (document.getElementById('baton2') !== null) {
  document.getElementById('baton2').onclick = function () {
    'use strict';
    document.getElementById('baton1').style.visibility = 'visible';
    document.getElementById('baton2').style.visibility = 'hidden';
    document.getElementById('baton').setAttribute('value', 'Счетчик');
  };
}

// ——————— по клику на кнопку красится параграф ——————— //
function colorThat() {
  'use strict';
  var r = Math.floor(Math.random() * 256), g = Math.floor(Math.random() * 256), b = Math.floor(Math.random() * 256);
  document.getElementById('text').style.color = 'rgb(' + r + ',' + g + ',' + b + ')';
}

// ——————— практика с confirm, prompt и alert ——————— //
function modalView() {
  'use strict';
  var answer = confirm('Уверен?'), name;

  if (answer === true && answer !== null) {
    name = prompt('Докажи, что ты человек. Напиши свое имя', '');
//    myAlert('Привет, ' + name);
    alert('Привет, ' + name);
  } else {
//    myAlert('Ну, блин.');
    alert('Ну, блин.');
  }
}

// ——————— условные операторы ——————— //
function question() {
  'use strict';
  var quest = prompt('Хау мач из дет фиш?', '100 500 руб');

  if (quest === '100 500 руб') {
//    myAlert('Да, все так.');
    alert('Да, все так.');
  } else {
//    myAlert('Nope');
    alert('Ноу.');
  }
}

function question2() {
  'use strict';
  var numb = prompt('Число какое-нибудь запили', '');

  if (numb > 0) {
//    myAlert('1');
    alert('1');
  } else if (numb < 0) {
//    myAlert('-1');
    alert('-1');
  } else if (numb === null) {
//    myAalert('Надо что-то вписать, друг.');
    alert('Надо что-то вписать, друг.');
  } else if (numb === 0) {
//    myAlert('0');
    alert('0');
  } else {
//    myAlert('Ноу.');
    alert('Ноу.');
  }
}

// ——————— условные операторы. тернарный оператор '?' ——————— //

//function question3 () {
//  var numb = prompt('Сколько будет 2+2?', '');
//  var a = b = 2;
//  if ( numb == (a + b) ) {
//    alert('Ага');
//  } else {
//    alert( 'Ебать ты лох' );
//  }
//};
//функция ниже — тоже самое, что и с if выше, но только с тренарным '?'

function question3() {
  'use strict';
  var numb = prompt('Сколько будет 2+2?', '');
  
//  myAlert(numb !== '4' ? 'Ну зачем так' : 'Ага');
  alert(numb !== '4' ? 'Ну зачем так' : 'Ага');
}

// несколько тренарных операторов. типа if, else if, else

//var message;
//if (login == 'Вася') {
//  message = 'Привет';
//} else if (login == 'Директор') {
//  message = 'Здравствуйте';
//} else if (login == '') {
//  message = 'Нет логина';
//} else {
//  message = '';
//}

// пример выше — тоже самое, что и пример ниже только вместо if else тренарный '?'

//var message = (login == 'Вася') ? 'Привет' :
//              (login == 'Директор') ? 'Здравствуйте' :
//              (login == '') ? 'Нет логина' : '';


// ——————— логические операторы || (ИЛИ), && (И) и ! (НЕ) ——————— //

//if ( !(age >= 14 && age <= 90) ) {}; 
//if ( age <= 14 && age >= 90) {};
//оба if абсолютно одинаковы. только первый с оператором ! НЕ, а второй без


// ——————— Циклы while, for ——————— //

//for (var i = 0; i < 3; i++) {
//  alert("number " + i + "!");
//}

// сверху и снизу код делает одно и то же

//var i = 0;

//while ( i < 3 ) {
//  alert("number " + i + "!");
//  i++;
//}

function question4() {
  'use strict';
  var numb;

  do {
    numb = prompt('Введите число больше 100', '');
  } while (numb <= 100 && numb !== true);
  alert('Красава.');
}

// ——————— 8. Директивы break, continue и метки. ——————— //
function question5() {
  'use strict';
  var i, j, input;
  outer: //якорь метки
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      input = prompt('Значение в координатах ' + i + ', ' + j, '');
      if (input === null) {
        break outer; // метка
      }
    }
  }
  alert('Норм');
}

function question6() {
  'use strict';
  var numb = prompt('Введите число больше 2', '10'), i, j;
  op:
  for (i = 2; i < numb; i++) {
    for (j = 2; j < i; j++) {
      if (i % j === 0) {
        continue op;
      }
    }
    alert(i);
  }
}

// ——————— 10. Функции. ——————— //
function question7() {
  'use strict';
  var age = prompt('Сколько лет?', '');
  alert(age >= 18 ? age + ' ( * ) ( * ) тебе за это!' : age + '? ' + 'Малой еще.\nА если мамка узнает?');
}

//function returnMin(a, b) {
//  return a > b ? a : b;
//}
//
//alert(returnMin(2, 5));

function showPrompt() {
  'use strict';
  var x = prompt('Введите число', ''), n = prompt('Введите степень числа', '');

  function pow(x, n) {
    var result = x, i;

    for (i = 1; i < n; i++) {
      result *= x;
    }
    return result;
  }
  if (n <= 1) {
    alert('Степень ' + n + 'не поддерживается, введите целую степень, больше 1');
  } else {
    alert(pow(x, n));
  }
}

// ——————— 12. Рекурсия. ——————— //
// через цикл
function question8() {
  'use strict';
  var n = prompt('Введите число', '100');

  function sumTo(n) {
    var sum = 0, i;

    for (i = 1; i <= n; i++) {
      sum += i;
    }
    return sum;
  }
  alert(sumTo(n));
}

//через рекурсию
function question9() {
  'use strict';
  function sumTo(n) {
    if (n === 1) {
      return 1;
    }
    return n + sumTo(n - 1);
  }
  alert(sumTo(100)); //5050
}

//с использованием арифметической прогрессии
function question10() {
  'use strict';
  function sumTo(n) {
    return n * (n + 1) / 2;
  }
  alert(sumTo(100));
}

//нахождение факториала
function question11() {
  'use strict';
  var n = prompt('Введите число', '5');

  function factorial(n) {
    return n ? n * factorial(n - 1) : 1;
  }
  alert(factorial(n));
}

//нахождение числа Фибоначчи
// черезе рекурсию
function question12() {
  'use strict';
  var n = prompt('Введите число', '10');

  function fibonachchi(n) {
    return n <= 1 ? n : fibonachchi(n - 1) + fibonachchi(n - 2);
  }
  alert(fibonachchi(n));
}

// по-матиматике просто
function question13() {
  'use strict';
  var n = prompt('Введите число', '10');

  function fibonachchi(n) {
    var a = 1, b = 1, c, i;
    c = a + b;
    for (i = 3; i <= n; i++) {
      c = a + b;
      a = b;
      b = c;
    }
    return b;
  }
  alert(fibonachchi(n));
}

// ——————— 13. Строка. ——————— //
function question14() {
  'use strict';
  var d = document.getElementById('input14');
  var sLen = d.value.length;

  d.value.toLowerCase().indexOf('пизда') > -1 ? alert('А по губам?') :
  d.value.toLowerCase().indexOf('хуй') > -1 ? alert('А по губам?') :
  sLen > 0 ? d.value = d.value.slice(0, 1).toUpperCase() + d.value.slice(1).toLowerCase() + '.' : alert('Поле не должно быть пустым.');
}

function question15() {
  'use strict';
  var input = document.getElementById('input15');
  var textarea = document.getElementById('textarea15');
  var tl = textarea.value.length;

  tl > input.value ? textarea.value = textarea.value.slice(0, input.value - 1) + '…' : textarea.value;
}

// ——————— 14. Числа. ——————— //
function question16() {
  'use strict';
  var input1 = document.getElementById('input16');
  var input2 = document.getElementById('input16_2');
  var input3 = document.getElementById('input16_3');

  input3.value = parseFloat(+input1.value) + parseFloat(+input2.value);
  input3.value.length >= 3 ? input3.value = Math.round(input3.value * 100) / 100 : input3.value;
  isNaN(input3.value) ? input3.value = 'error' : input3.value;
}

function question17() {
  'use strict';
  var n = document.getElementById('input17');
  var result = document.getElementById('input17_2');

  var a = (1 + Math.sqrt(5)) / 2;
  var b = (1 - Math.sqrt(5)) / 2;

  result.value = Math.floor( (Math.pow(a, n.value) - Math.pow(b, n.value) ) / Math.sqrt(5) );
}

function question18() {
  'use strict';
  var min = 5, max = 25, result;
  result = document.getElementById('input18');
  result.value = Math.floor(Math.random() * (max - min + 1) + min);
}

// ——————— 15. Объекты. Ассоциативные массивы. ——————— //
var box15 = {}

box15['anyShit'] = 's500';
box15['width'] = 500;
box15['height'] = 300;
box15['unit'] = 'px';

function multiplyNumber(obj) {
  'use strict';
  for(var key in obj) {
    if (isNumber(obj[key])) {
      var n = obj[key] * 2;
      alert(n);
    }
  }
}

function question19() {
  multiplyNumber(box15);
}

// ——————— 16. Массивы. ——————— //
//var goods = [1,2,3,4,5,6,'sdf']; //синтаксис массива
//var n = goods.length;
//
//alert(goods[n-1]); //вывел последний элемент массива
//goods[n] = 'zvizda'; //добавил новый элемент. n — индекс элемента в массиве. отсчет начинается с нуля (0)
//alert(goods.length); //вывел длину массива
//goods.pop();       //удалил последний элемент и возвратил его
//goods.push('asf'); //добавил элемент в конец массива
//goods.shift(); //удалил элемент из начала массива
//goods.unshift('12'); //добавил элемент в начало массива
//alert(goods); //вывел все элементы массива

// код для рандомного вывода элемента из массива
//var arra = ["Яблоко", "Апельсин", "Груша", "Лимон"];
//var rand = 0 + Math.floor( Math.random() * (arra.length - 1 + 1 - 0) );
//alert(arra[rand] + rand);

var promptArr = [];

//эта функция пишет данные в произвольный массив, через prompt
function pushingPropmt(parr) {
  'use strict';
  var i = 0;

  while(parr[parr.length - 1] !== null) { //если пустая строка или отмена, то заканчиваем цикл
    parr[i] = prompt('Введите число', '13'); //пишем числа в массив
    if(isNumber(parr[parr.length - 1]) === false) break; //если не число, то прерываем цикл
    parr.push(' + '); //на каждом прогоне после числа добавляем знак + в массив
    i = i + 2; 
  }
  parr.pop(); //чистим от null
  parr.pop(); //чистим от лишнего плюса перед null
  return parr; //возвращаем набитый числами массив
}

//при нажатии на кнопку вызывается эта функция
//из нее вызывается другая функция, в которую возвращается массив с набором чисел и знаков +
function question20() {
  'use strict';
  pushingPropmt(promptArr); //вызвали функцию для массива promptArr[]

  var sumArr = 0;
  for (var i = 0; i < promptArr.length; i++) { //цикл для подсчета суммы всех чисел из массива
    if(isNumber(promptArr[i]) === false) continue; //проверка на число. если не число(если +), то пропускаем значение
    sumArr += +promptArr[i]; //пишем в переменную сумму
  }

  var result = document.getElementById('input20');
  var clearSum = sumArr + ' = ' + promptArr.join(''); //переменная с выражением суммы. 123 = 100 + 20 + 3, например

  //если количество символов больше, чем надо, то обрезаем и ставим …
  //если не превышает, то выводим результат как есть 
  clearSum.length > 68 ? result.value = clearSum.slice(0, 68) + '…' : result.value = clearSum;

  //проверка на ноль в сумме
  if (sumArr == 0) {
    result.value = 'нужны реальные числа';
  }

  //чистим массив
  promptArr.length = 0;
}

//функция поиска элемента массива с выведением его индекса
//function find(array, value) {
//  var i;
//	for(i = 0; i < array.length; i++) {
//		if(array[i] === value) return i;
//	}
//  return -1;
//}
//
//var arrows = ['test', 2, 1.5, false];
//
//alert(find(arrows, 'test'));
//alert(find(arrows, 2));
//alert(find(arrows, 1.5));
//alert(find(arrows, 0));

//функция, которая принимает массив и возвращает новый с набором чисел из первого но с диапазоном от А до Б
//function filterRange(arr, a, b) {
//  var newArr = [], i, j = 0;
//  for(i = a; i <= b; i++) {
//    newArr[j] = arr[i];
//    j++;
//  }
//  return newArr;
//}

//var array2 = [1,2,3,4,5,6,7,8,9,10];
//alert(filterRange(array2, 3, 7)); //новый массив с числами от 3 до 7 индексов
//alert(array2); //старый массив не изменился

//функция "решето Эратосфена"
function eratosfen(arr, n) {
  var i, j, x, tempArr, sum;
  for (i = 2; i <= n; i++) {
    arr[i] = i;
  }
  j = 2;
  do {
    for (i = 2 * j; i < n; i += j) {
      arr[i] = 1;
    }
    for (i = 1 + j; i < n; i++) {
      if(arr[i]) break;
    }
    
    j = i;
  } while (j * j < n);
  arr.shift();
  arr.shift();

  for(i = 0; i < arr.length; i++) {
    x = 0;
    tempArr = [];
    if(arr[i] === 1) {
      for (j = i + 1; j < arr.length; j++) {
        tempArr[x] = arr[j];
        x++;
      }
      arr.length = i;
      x = i;
      for(j = 0; j < tempArr.length; j++) {
        arr[x] = tempArr[j];
        x++;
      }
      i = 0;
    }
  }
  
  sum = 0;
  for (i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  alert(arr);
  alert(sum);
}

function question21() {
  var array = [];
  eratosfen(array, document.getElementById('input21').value);
}

// ——————— 17. Массивы: методы. ——————— //
//метод Object.keys(obj), который возвращает ключи объекта
//var user = {
//  name: 'Huy',
//  age: 25
//}
//var someKeys = Object.keys(user);
//alert(someKeys);

function unique(arr) {
  var tempObj, i, someKeys;
  tempObj = {};
  
  for(i = 0; i < arr.length; i++) {
    tempObj[arr[i]] = arr[i];
  }
  return Object.keys(tempObj);
}

function question22() {
  var strings = [' белый', ' белый', ' красный', ' синий', ' зеленый', ' красный', ' синий'];
  alert(unique(strings));
}


function addText(obj, text) {
  var str = obj.yourText ? obj.yourText.split(' ') : [];
  
  for(var i = 0; i < str.length; i++) {
    if(str[i] === text) return;
  }
  str.push(text);
  obj.yourText = str.join(' ');
  document.getElementById('p23').innerHTML = obj.yourText;
}
var objForP23 = {
  yourText: 'Тут будет ваш текст:'
}
function question23() {
  addText(objForP23, document.getElementById('input23').value);
  document.getElementById('input23').value = '';
}
function question23_2() {
  document.getElementById('p23').innerHTML = 'Тут будет ваш текст:';
  objForP23.yourText = 'Тут будет ваш текст:';
}


//с помощью indexOf
function removeText(obj, text) {
  var str, index;
  str = obj.yourText;
  
  if(str.indexOf(text) > -1) {
    index = str.indexOf(text);
    str = str.slice(0, index) + str.slice(index + text.length, str.length);
  } else {
    return;
  }
  
  obj.yourText = str;
  document.getElementById('p24').innerHTML = obj.yourText;
}
var objForP24 = {
  yourText: 'Из этого текста будет удаляться то, что вы введете в инпут выше. Например, наберите вот эти четыре слова. Или можете вписать что-то, чего нет в этом тексте. Тогда ничего не произойдет. Если вы уничтожили весь текст, то нажмите Восстановить.'
}
function question24() {
  removeText(objForP24, document.getElementById('input24').value);
  document.getElementById('input24').value = '';
}
function question24_2() {
  document.getElementById('p24').innerHTML = 'Из этого текста будет удаляться то, что вы введете в инпут выше. Например, наберите вот эти четыре слова. Или можете вписать что-то, чего нет в этом тексте. Тогда ничего не произойдет. Если вы уничтожили весь текст, то нажмите Восстановить.';
  objForP24.yourText = 'Из этого текста будет удаляться то, что вы введете в инпут выше. Например, наберите вот эти четыре слова. Или можете вписать что-то, чего нет в этом тексте. Тогда ничего не произойдет. Если вы уничтожили весь текст, то нажмите Восстановить.';
  document.getElementById('input24').value = 'вот эти четыре слова';
}


//с помощью splice
function removeText2(obj, text) {
  var arr;
  arr = obj.yourText.split(' ');
 
  for(i = 0; i < arr.length; i++) {
    if (arr[i] === text) {
      arr.splice(i, 1); // удаляем текст
      // так как удалили ячейку, то все сдвинулось, чтобы ее заполнить
      // и чтобы не пропустить следующую ячейку, нужно задикрементить счетчик
      // и тем самым продолжить перебирать массив с того же места
      i--;
    }
  }
  obj.yourText = arr.join(' ');
  document.getElementById('p25').innerHTML = obj.yourText;
}
var objForP25 = {
  yourText: 'Для данной задачи этот способ непрактичен, потому что в массиве весь текст лежит по одному слову на индекс. То есть удалять можно только одно слово за раз.'
}
function question25() {
  removeText2(objForP25, document.getElementById('input25').value);
  document.getElementById('input25').value = '';
}
function question25_2() {
  document.getElementById('p25').innerHTML = 'Для данной задачи этот способ непрактичен, потому что в массиве весь текст лежит по одному слову на индекс. То есть удалять можно только одно слово за раз.';
  objForP25.yourText = 'Для данной задачи этот способ непрактичен, потому что в массиве весь текст лежит по одному слову на индекс. То есть удалять можно только одно слово за раз.';
  document.getElementById('input25').value = 'способ непрактичен';
}


//удаление всех чисел вне диапазона от a до b не включая a и b
//function filterRangeInPlace(arr, a, b) {
//  var i;
//  for(i = 0; i < arr.length; i++) {
//    if(arr[i] < a || arr[i] > b) {
//      arr.splice(i--, 1);
//    }
//  }
//}
//
//var arrNumbers = [1, 2, 3, 4, 6, 7, 8, 9, 10, 5, 3, 8, 1];
//filterRangeInPlace(arrNumbers, 1, 4);
//alert(arrNumbers);


//своя сортировка
//function sortArr(arr) {
//  arr.sort(function reversSort(a, b) {return b - a;});
//}
//
//var arrNumbers = [1, 2, 3, 4, 6, 7, 8, 9, 10, 5, 3, 8, 1];
//sortArr(arrNumbers);
//alert(arrNumbers); // обратный порядок 10,9,8,8,7,6,5,4,3,3,2,1,1

//сортировка строк
//function sortStrokeArr(arr) {
//  var newArr;
//  newArr = arr.slice().sort();
//  alert(arr);
//  alert(newArr);
//}
//
//var strokeArr = ['HTML', 'JavaScript', 'CSS'];
//sortStrokeArr(strokeArr);


//reverse
//вывод всех значений
/*function showList2(list) {
  var tmp;
  tmp = list;
  
  while (tmp) {
    alert(tmp.value);
    tmp = tmp.next;
  }
}*/

//вывод значений с реверсом через рекурсию
/*function showReverseList(list) {
  if (list.next) {
    showReverseList(list.next);
  }
  alert(list.value);
}*/

//вывод значений с реверсом без рекурсии
/*function showReverseListWithoutR(list) {
  var i, tmp, arr = [];
  tmp = list;
  
  while (tmp) {
    arr.push(tmp.value);
    tmp = tmp.next;
  }
  arr.reverse();
  for (i = 0; i < arr.length; i++) {
    alert(arr[i]);
  }
}*/

/*var objFor26 = {
  value: 1, next: {
    value: 2, next: {
      value: 3, next: {
        value: 4, next: null
      }
    }
  }
};*/
//showList2(objFor26);
//showReverseList(objFor26);
//showReverseListWithoutR(objFor26);


//очистка от анаграмм
/*function cleanAnagram(arr) {
  var i, str, tempArr, tempObj, key;
  tempObj = {};
  tempArr = [];
  for (i = 0; i < arr.length; i++) {
    str = arr[i].toLowerCase().split('').sort().join('');
    tempObj[str] = arr[i];
  }
  for (key in tempObj) {
    tempArr.push(tempObj[key]);
  }
  for (i = 0; i < tempArr.length; i++) {
    tempArr[i] = tempArr[i].toLowerCase();
  }
  alert(tempArr);
}

var arrAnagram = ['карат', 'КИЛЬВАТЕР', 'слово', 'карта', 'ВЕРтикаль', 'волос', 'кабан', 'банка'];
cleanAnagram(arrAnagram);*/

function setDateWork(parag) {
  var date, par;
  date = new Date;
  par = document.getElementById(parag);
  par.innerHTML = 'Время на момент загрузки страницы: <span class="pre">' + date + '</span>.';
}

if (document.getElementById('parag-1') !== null) {
  setDateWork('parag-1');
}

function setDateWorkFromDate(parag) {
  var date, par;
  date = new Date(1000 * 3600 * 48); //48 часов от 1 января 1970 года
  par = document.getElementById(parag);
  par.innerHTML = '2 дня после 1 января 1970 года: <span class="pre">' + date + '</span>.';
}

if (document.getElementById('parag-2') !== null) {
  setDateWorkFromDate('parag-2');
}

if (document.getElementById('question-26') !== null) {
  document.getElementById('question-26').onclick = function () {
    var date;
    date = new Date();
    alert(date.getHours() + ':' + date.getMinutes());
    alert(date.getUTCHours() + ':' + date.getUTCMinutes());
  }
}

if (document.getElementById('question-27') !== null) {
  document.getElementById('question-27').onclick = function () {
    var date, arr;
    date = new Date();
    arr = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    
    alert(arr[date.getDay()]);
  }
}

if (document.getElementById('question-28') !== null) {
  document.getElementById('question-28').onclick = function () {
    var date;
    date = new Date(2012, 0, 8).getDay();
    if (date === 0) {
      date = 7;
    }
    alert('8 января 2012 — ' + date + ' день недели, воскресение');
  }
}